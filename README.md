# Deployment Containers

This repository contains Dockerfiles and build tools to build deployment containers.


## Building

All images in this repository are built manually. You must go into the pipeline and
activate any jobs you wish to be built.


### Helm 3

You must specify the Kubernetes version to be built using the `KUBE_VERSION` variable
in the Gitlab **Run Pipeline** interface.

Versions currently built include:
- `1.23.5`
