#!/bin/sh

if [ -f "/var/run/secrets/kubernetes.io/serviceaccount/token" -a ! -f "/root/.kube/config" ]; then
    KUBERNETES_CA_CRT=$(base64 /var/run/secrets/kubernetes.io/serviceaccount/ca.crt | xargs echo | sed 's/ //g')
    KUBERNETES_TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)

    mkdir -p /root/.kube
    cat <<__EOF__ > /root/.kube/config
apiVersion: v1
kind: Config
clusters:
- name: default-cluster
  cluster:
    certificate-authority-data: "${KUBERNETES_CA_CRT}"
    server: "https://${KUBERNETES_SERVICE_HOST}"
contexts:
- name: default-context
  context:
    cluster: default-cluster
    namespace: "${KUBERNETES_NAMESPACE}"
    user: default-user
users:
- name: default-user
  user:
    token: "${KUBERNETES_TOKEN}"
current-context: default-context
__EOF__
    chmod 0600 /root/.kube/config
fi

if [ -n "${1:-}" ]; then
    exec "$@"
else
    exec "/bin/sh"
fi
